from django.shortcuts import render,render_to_response
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.views import generic
from parse_rest.datatypes import Object
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
import json,httplib
from .forms import T1VObjectForm
from django.views.generic.edit import FormView
from django.core.context_processors import csrf
from django.template import RequestContext
from django_boto.s3 import upload
import uuid


from .models import T1VClassField, T1VClass, T1VClassParse

class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'
    
    def get_queryset(self):
        """Return the last five published questions."""
        #fetch classes from parse
        current_user = self.request.user
        print(current_user)
        return T1VClass.objects.order_by('-pub_date')[:5]
    
    @method_decorator(login_required(login_url='/accounts/login/'))
    def dispatch(self, *args, **kwargs):
        return super(IndexView, self).dispatch(*args, **kwargs)

def detail(request, t1vclass_id):
	context_instance = RequestContext(request)
	print("----------- Detail View Called------------")

	if request.method == 'POST':
		form = T1VObjectForm(request.POST or None, class_id=t1vclass_id, delete_check = 'true')
		print(request.FILES)
		if form['should_delete'] == True :
			# get the object_id from context
			object_id = request['object_id'];
			print(object_id)
		saveobject(request,form, t1vclass_id)
		return HttpResponseRedirect(reverse('polls:results', args=(t1vclass_id,)))
	else:
		print("------- GET called -----------")
		result = request.GET.get('result')
		print(result)
		form = T1VObjectForm(None,class_id=t1vclass_id,delete_check='false')
		#if its coming from results page by clicking on object link
# 		if object_id != 'nil':
# 			p = get_object_or_404(T1VClass, pk=t1vclass_id)
# 			class_name = p.class_name
# 			post_request_string = "/1/classes/"+class_name+"/"+str(object_id)
# 			connection = httplib.HTTPSConnection('api.parse.com', 443)
# 			connection.connect()
# 			connection.request('GET', post_request_string, '', 
# 			{"X-Parse-Application-Id": "PyEZIa21SrEDydjXrv1hqUCg2wxw6KxRlDBaZpBH","X-Parse-REST-API-Key": "McmlbzpHcfPNQgzy3GMwow78uR9e91u8o4VFleKX" })
# 			result = json.loads(connection.getresponse().read())
# 			print result
# 			for t1vclassfield in p.t1vclassfield_set.all():
# 				field_name = t1vclassfield.field_name
# 				form.fields[field_name].initial = str(result[field_name])
	return render_to_response("polls/detail.html", {'form': form}, context_instance)

  

def saveobject(request,form, t1vclass_id):
	for key, value in request.FILES.iteritems():
		file_upload_url = handle_uploaded_file(request.FILES[key])
	print("-----saving object------")
	p = get_object_or_404(T1VClass, pk=t1vclass_id)
	class_name = p.class_name
	class_dictionary = {}
	post_request_string = "/1/classes/"+class_name
	print(request.FILES)
	
	#required to generate the cleaned data attributes of form. We really dont care how the data comes now :| 
	form.is_valid()	
	
	for t1vclassfield in p.t1vclassfield_set.all():
		field_name = t1vclassfield.field_name
		field_type = t1vclassfield.field_type
		field_value = form.cleaned_data[str(field_name)]
		if field_type != "file":
			class_dictionary[str(field_name)] = str(field_value)
		else:
			class_dictionary[str(field_name)] = file_upload_url;
	#post class to parse- couldnt find a method that would conveniently put up an object in the json structure to parse

	connection = httplib.HTTPSConnection('api.parse.com', 443)
	connection.connect()
	connection.request('POST', post_request_string, json.dumps(class_dictionary), {
       "X-Parse-Application-Id": "PyEZIa21SrEDydjXrv1hqUCg2wxw6KxRlDBaZpBH",
       "X-Parse-REST-API-Key": "McmlbzpHcfPNQgzy3GMwow78uR9e91u8o4VFleKX",
       "Content-Type": "application/json"
     })
	results = json.loads(connection.getresponse().read())


# def saveobject(request, t1vclassid):
# 	p = get_object_or_404(T1VClass, pk=t1vclass_id)
# 	class_name = p.class_name
# 	return HttpResponseRedirect(reverse('polls:results', args=(p.id,)))


# def updateobject(request, t1vclass_id, object_id):
# 	t1vclass = get_object_or_404(T1VClass, pk=t1vclass_id)
# 	#fetch from parse the object
# 	p = get_object_or_404(T1VClass, pk=t1vclass_id)
# 	class_name = p.class_name
# 	post_request_string = "/1/classes/"+class_name+"/"+str(object_id)
# 	print(post_request_string)
# 	class_dictionary = {}
# 	for t1vclassfield in p.t1vclassfield_set.all():
# 		field_name = t1vclassfield.field_name
# 		field_value = request.POST.get(str(t1vclassfield.id))
# 		class_dictionary[str(field_name)] = str(field_value)
# 	should_delete = request.POST.get('delete')
# 	
# 	connection = httplib.HTTPSConnection('api.parse.com', 443)
# 	connection.connect()
# 	
# 	if should_delete == "yes":
# 		connection.request('DELETE', post_request_string, '', {
#        "X-Parse-Application-Id": "PyEZIa21SrEDydjXrv1hqUCg2wxw6KxRlDBaZpBH",
#        "X-Parse-REST-API-Key": "McmlbzpHcfPNQgzy3GMwow78uR9e91u8o4VFleKX"
#      })
# 	else:
# 		print(class_dictionary)
# 		print(json.dumps(class_dictionary))
# 		#post class to parsePy - couldnt find a method that would conveniently put up an object in the json structure to parse
# 
# 
# 		connection.request('PUT', post_request_string, json.dumps(class_dictionary), {
# 		   "X-Parse-Application-Id": "PyEZIa21SrEDydjXrv1hqUCg2wxw6KxRlDBaZpBH",
# 		   "X-Parse-REST-API-Key": "McmlbzpHcfPNQgzy3GMwow78uR9e91u8o4VFleKX",
# 		   "Content-Type": "application/json"
# 		 })
# 
# 	results = json.loads(connection.getresponse().read())
# 	print results
# 	return HttpResponseRedirect(reverse('polls:results', args=(p.id,)))
	
def results(request, t1vclass_id):
    t1vclass = get_object_or_404(T1VClass, pk=t1vclass_id)
    #fetch from parse the objects
    myClass = Object.factory(t1vclass.class_name)
    all_objects = myClass.Query.all()
    return render(request, 'polls/results.html', {'t1vclass': t1vclass,'myClass':myClass,'all_objects':all_objects })
 
 #populates fields with values when user clicks on object ids, deletes objects when user submits with delete checkbox checked, saves objects when they are modified 
def edit(request, t1vclass_id, object_id):
	t1vclass = get_object_or_404(T1VClass, pk=t1vclass_id)
	#fetch from parse the object
	p = get_object_or_404(T1VClass, pk=t1vclass_id)
	class_name = p.class_name
	parse_request_string = "/1/classes/"+class_name+"/"+str(object_id)
	connection = httplib.HTTPSConnection('api.parse.com', 443)
	connection.connect()
	
	#get the class
	print("------Edit getting called ------")
	if request.method == 'GET':
		connection.request('GET', parse_request_string, '', 
		{"X-Parse-Application-Id": "PyEZIa21SrEDydjXrv1hqUCg2wxw6KxRlDBaZpBH","X-Parse-REST-API-Key": "McmlbzpHcfPNQgzy3GMwow78uR9e91u8o4VFleKX" })
	
		result = json.loads(connection.getresponse().read())
		print result
		form = T1VObjectForm(class_id=t1vclass_id, delete_check='true')
		
		file_links_dictionary = {}
		for t1vclassfield in p.t1vclassfield_set.all():
			field_name = t1vclassfield.field_name
			field_type = t1vclassfield.field_type
			if field_type != 'file':
				form.fields[field_name].initial = str(result[str(field_name)])
			else:
				if field_name in result:
					file_links_dictionary[str(field_name)] = result[str(field_name)]
					print result[str(field_name)]
				else:
					print ("No entry for",field_name)
		#add object_id to context instance
		context_instance = RequestContext(request,{'object_id':object_id,'file_links':file_links_dictionary})
		return render_to_response("polls/detail.html", {'form': form},context_instance) 
	elif request.method == 'POST':
		#get the delete button
		form = T1VObjectForm(request.POST, class_id=t1vclass_id, delete_check = 'true')
		class_name = p.class_name
		#required to generate the cleaned data attributes of form :|
		form.is_valid()
		file_upload_url = None
		should_delete = form.cleaned_data['should_delete']
		if should_delete == True :
			connection.request('DELETE', parse_request_string, '', {"X-Parse-Application-Id": "PyEZIa21SrEDydjXrv1hqUCg2wxw6KxRlDBaZpBH","X-Parse-REST-API-Key": "McmlbzpHcfPNQgzy3GMwow78uR9e91u8o4VFleKX"})
			return HttpResponseRedirect(reverse('polls:results', args=(p.id,)))
		else:
			class_dictionary = {}
			for key, value in request.FILES.iteritems():
				file_upload_url = handle_uploaded_file(request.FILES[key])
			for t1vclassfield in p.t1vclassfield_set.all():
				field_name = t1vclassfield.field_name
				field_type = t1vclassfield.field_type
				field_value = form.cleaned_data[str(field_name)]
				if field_type != "file":
					class_dictionary[str(field_name)] = str(field_value)
				else:
					if not file_upload_url == None:
						class_dictionary[str(field_name)] = file_upload_url
			connection = httplib.HTTPSConnection('api.parse.com', 443)
			connection.connect()
			connection.request('PUT', parse_request_string, json.dumps(class_dictionary), {
			   "X-Parse-Application-Id": "PyEZIa21SrEDydjXrv1hqUCg2wxw6KxRlDBaZpBH",
			   "X-Parse-REST-API-Key": "McmlbzpHcfPNQgzy3GMwow78uR9e91u8o4VFleKX",
			   "Content-Type": "application/json"
			 })
			return HttpResponseRedirect(reverse('polls:results', args=(p.id,)))

	
 	
#  	return HttpResponseRedirect(reverse('polls:detail', args=(t1vclass_id,result)))
# 	return render(request, 'polls/detail.html', {'t1vclass': t1vclass, 'result_object':result })
	
def handle_uploaded_file(f):
	print ("file retreived by upload")
	file_uuid_for_s3 = uuid.uuid1().hex
	file_uuid_for_s3 += str(f.content_type)
	s3_file_URL = upload(f, name=file_uuid_for_s3, prefix=False, bucket_name="t1vcloud", key=None, secret=None, 
		host=None, expires=0, query_auth=False, force_http=True,policy=None)
	return s3_file_URL


        
