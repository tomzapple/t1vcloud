# In forms.py...
from django import forms
from .models import T1VClassField, T1VClass
from django.shortcuts import get_object_or_404
# from s3direct.widgets import S3DirectWidget

class T1VObjectForm(forms.Form):
# 	images = forms.URLField(label = "images", widget=S3DirectWidget(dest='imgs'))
	
	def __init__(self, *args, **kwargs):
		t1vclass_id = kwargs.pop('class_id')
		should_add_delete_button = kwargs.pop('delete_check')
		super(T1VObjectForm, self).__init__(*args, **kwargs)
		p = get_object_or_404(T1VClass, pk=t1vclass_id)										
		for t1vclassfield in p.t1vclassfield_set.all():
			field_name = t1vclassfield.field_name
			field_type = t1vclassfield.field_type
			if field_type == 'string':
				self.fields[field_name] = forms.CharField(label = field_name, required=False)
			if field_type in ['geopoint','array','object']:
				self.fields[field_name] = forms.SlugField(label = field_name, required=False)			
			if field_type == 'number':
				self.fields[field_name] = forms.IntegerField(label = field_name, required=False)
			if field_type == 'boolean':
				self.fields[field_name] = forms.BooleanField(label = field_name, required=False)
			if field_type == 'date':
				self.fields[field_name] = forms.DateField(label = field_name, required=False)	
			if field_type == 'file':
				print(field_name)
				self.fields[field_name] = forms.FileField(label = field_name, required=False)
		if should_add_delete_button == 'true':
			self.fields['should_delete'] = forms.BooleanField(label = "Delete",required=False, initial=False)
			
# 	def clean(self):
# 		super(T1VObjectForm, self).clean() #if necessary
# 		print(self.cleaned_data)
# 		print(self._errors)
# 		return self.cleaned_data