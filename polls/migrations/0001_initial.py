# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='T1VClass',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('class_name', models.CharField(max_length=200)),
                ('pub_date', models.DateTimeField(verbose_name=b'date published')),
            ],
        ),
        migrations.CreateModel(
            name='T1VClassField',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('field_name', models.CharField(max_length=200)),
                ('field_type', models.CharField(max_length=10, choices=[(b'string', b'String'), (b'number', b'Number'), (b'boolean', b'Boolean'), (b'date', b'Date'), (b'file', b'File'), (b'geopoint', b'GeoPoint'), (b'array', b'Array'), (b'object', b'Object')])),
                ('t1vclass', models.ForeignKey(to='polls.T1VClass')),
            ],
        ),
    ]
