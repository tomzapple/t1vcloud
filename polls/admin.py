from django.contrib import admin
from .models import T1VClass, T1VClassField


class T1VClassFieldInline(admin.TabularInline):
    model = T1VClassField
    extra = 2

class QuestionAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['class_name']}),
    ]
    inlines = [T1VClassFieldInline]
    list_display = ('class_name', 'pub_date', 'was_published_recently')
    list_filter = ['pub_date']
    search_fields = ['class_name']

admin.site.register(T1VClass, QuestionAdmin)