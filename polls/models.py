import datetime
from django.db import models
from django.utils import timezone

from django.db.models.signals import post_save
from django.dispatch import receiver

from parse_rest.datatypes import Object
from django.shortcuts import get_object_or_404
from django.template.defaulttags import register

class T1VClass(models.Model):
    class_name = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published', default=datetime.datetime.now)
    
    def __str__(self):              # __unicode__ on Python 2
        return self.class_name
        
    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)
    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published recently?'
    
#     def update_class(sender, instance, **kwargs):
#      	print("------model saved-------")
#     def my_callback(sender, **kwargs):
#     	if kwargs['created']:
#         	print('Instance is new')
#      
# 	post_save.connect(my_callback, sender=T1VClass, dispatch_uid="update t1vClass")  
	
class T1VClassParse(Object):
    pass
    
class T1VClassFieldParse(Object):
    pass
    	
@receiver(post_save, sender=T1VClass)
def my_handler(sender, instance, **kwargs):
	print("-------Class Created Created ---------")
	print (instance.class_name)
	t1vc = T1VClass.objects.get(pk = instance.id)
	print(t1vc.id)
	t1vparseclassfields = T1VClassFieldParse.Query.filter(class_name=instance.t1vclass.class_name)		
	t1vclass = T1VClassParse(name = instance.class_name)
	t1vclass.fields = t1vparseclassfields;
	t1vclass.save()
	

class T1VClassField(models.Model):
    t1vclass = models.ForeignKey(T1VClass)
    field_name = models.CharField(max_length=200)

    CLASS_FIELD_TYPE_CHOICES = (
        ('string', 'String'),
        ('number', 'Number'),
        ('boolean', 'Boolean'),
        ('date', 'Date'),
        ('file', 'File'),
        ('geopoint', 'GeoPoint'),
        ('array', 'Array'),
        ('object', 'Object'),
    )
    field_type = models.CharField(max_length=10,
                                      choices=CLASS_FIELD_TYPE_CHOICES)
    html_input_type_map = {'string': "text", 'number': "number", "boolean" :"number", "date" : "date", "file":"file", 'geopoint':"text", 'array':"text", 'object':"text"}
                                       
    def __str__(self):              # __unicode__ on Python 2
        return self.field_name
        
        
@receiver(post_save, sender=T1VClassField)
def my_handler(sender, instance, **kwargs):
	print("Class Field created")
	parse_class_field = T1VClassFieldParse(field_name = instance.field_name, field_type = instance.field_type, class_name = instance.t1vclass.class_name)
	parse_class_field.save()

#get the respective class for the classfield from parse	
	t1vclasslist = T1VClassParse.Query.filter(name=instance.t1vclass.class_name)
	print (t1vclasslist)

# relate the class field to the class	
	if not t1vclasslist:
		t1vclass = T1VClassParse(name = instance.t1vclass.class_name)
		t1vclass.class_fields = []
		t1vclass.class_fields.append(parse_class_field)
		t1vclass.save()
	else:
		t1vclasslist[0].class_fields.append(parse_class_field)
		t1vclasslist[0].save()
		
@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)

# @register.filter
# def get_label_clean(label_tag):
#     return label_tag[:-1]
# 		
		

      