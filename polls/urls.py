from django.conf.urls import url, include
from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
    url('^', include('django.contrib.auth.urls')),
#     url(r'^s3direct/', include('s3direct.urls')),
    url(r'^$', views.IndexView.as_view(), name='index'),
	url(r'^accounts/login/$', 'django.contrib.auth.views.login'),
    url(r'^(?P<t1vclass_id>[0-9]+)/$', views.detail, name='detail'),
    url(r'^(?P<t1vclass_id>[0-9]+)/results/$', views.results, name='results'),
#     url(r'^(?P<t1vclass_id>[0-9]+)/saveobject/$', views.saveobject, name='saveobject'),
    url(r'^(?P<t1vclass_id>[0-9]+)/(?P<object_id>\w+)/edit/$', views.edit, name='edit'),
# 	url(r'^(?P<t1vclass_id>[0-9]+)/(?P<object_id>\w+)/updateobject/$', views.updateobject, name='updateobject'),
]
